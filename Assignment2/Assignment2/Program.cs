﻿using System;

namespace Assignment2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int[] numbers = new int[]{ 1, 3, 4, 5 };
            Calculator c = new Calculator(numbers);
            Console.WriteLine(c.Sum());
            Console.WriteLine(c.Division(4));
            Console.WriteLine("Hello World!");
        }
    }
}
